
//
//  FYSegment.swift
//  Crabs_star
//
//  Created by FishYan on 2017/12/20.
//  Copyright © 2017年 com.newlandcomputer.app. All rights reserved.
//

import UIKit
import MBProgressHUD

public func UIColorFromRGB(_ rgbValue: UInt) -> UIColor {
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

public func Toast(_ message: String) {
    MBProgressHUD.hide(for: UIApplication.shared.keyWindow!, animated: false)
    if message == "" {
        MBProgressHUD.hide(for: UIApplication.shared.keyWindow!, animated: false)
        return
    }
    let HUD = MBProgressHUD(view: UIApplication.shared.keyWindow!)
    UIApplication.shared.keyWindow!.addSubview(HUD)
    HUD.animationType = .zoomOut
    HUD.bezelView.color = UIColor.black
    HUD.contentColor = UIColor.white
    HUD.mode = .text
    HUD.label.text = message
    HUD.label.numberOfLines = 0
    HUD.show(animated: true)
    HUD.hide(animated: true, afterDelay: 2.0)
}


// MARK: - Loading
public func Loading() {
    guard let keyWidnow = UIApplication.shared.keyWindow else {
        return
    }
    RemoveLoading()
    let HUD = MBProgressHUD(view: keyWidnow)
    keyWidnow.addSubview(HUD)
    HUD.bezelView.color = UIColor.black
    HUD.tag = 1001
    HUD.contentColor = UIColor.white
    HUD.mode = .indeterminate
    HUD.show(animated: true)
}

public func RemoveLoading() {
    guard let keyWidnow = UIApplication.shared.keyWindow else {
        return
    }
    for view in keyWidnow.subviews {
        if view.tag == 1001 {
            view.removeFromSuperview()
        }
    }
}
