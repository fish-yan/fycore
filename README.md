# FYCore

[![CI Status](https://img.shields.io/travis/fish-yan/FYCore.svg?style=flat)](https://travis-ci.org/fish-yan/FYCore)
[![Version](https://img.shields.io/cocoapods/v/FYCore.svg?style=flat)](https://cocoapods.org/pods/FYCore)
[![License](https://img.shields.io/cocoapods/l/FYCore.svg?style=flat)](https://cocoapods.org/pods/FYCore)
[![Platform](https://img.shields.io/cocoapods/p/FYCore.svg?style=flat)](https://cocoapods.org/pods/FYCore)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FYCore is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'FYCore'
```

## Author

fish-yan, 757094197@qq.com

## License

FYCore is available under the MIT license. See the LICENSE file for more info.
